package br.com.empresa.models;

public class ReceitaCapital {
    private String nome;
    private String capital_social;

    public ReceitaCapital() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCapital_social() {
        return capital_social;
    }

    public void setCapital_social(String capital_social) {
        this.capital_social = capital_social;
    }
}
