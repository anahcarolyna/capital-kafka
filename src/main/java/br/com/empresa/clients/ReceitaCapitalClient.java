package br.com.empresa.clients;

import br.com.empresa.models.ReceitaCapital;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "capital", url="https://www.receitaws.com.br")
public interface ReceitaCapitalClient {

    @GetMapping("/v1/cnpj/{cnpj}")
    ReceitaCapital getById(@PathVariable String cnpj);
}
