package br.com.empresa.clients;

import br.com.empresa.exception.ErroServicoException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ReceitaCapitalDecoder implements ErrorDecoder{

    private ErrorDecoder errorDecoder = new ErrorDecoder.Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            return new ErroServicoException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
