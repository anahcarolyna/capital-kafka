package br.com.empresa.clients;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class ReceitaCapitalClientConfiguration {
    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new ReceitaCapitalDecoder();
    }
}
