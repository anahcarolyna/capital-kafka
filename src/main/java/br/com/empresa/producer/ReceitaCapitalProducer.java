package br.com.empresa.producer;

import br.com.empresa.clients.ReceitaCapitalClient;
import br.com.empresa.models.Empresa;
import br.com.empresa.models.ReceitaCapital;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ReceitaCapitalProducer {

    @Autowired
    private KafkaTemplate<String, ReceitaCapital> producer;

    @Autowired
    private ReceitaCapitalClient receitaCapitalClient;

    public void enviarAoKafka(Empresa empresa) {
        ReceitaCapital receitaCapital = receitaCapitalClient.getById(empresa.getCnpj());
        producer.send("spec3-ana-carolina-3", receitaCapital);
    }
}
