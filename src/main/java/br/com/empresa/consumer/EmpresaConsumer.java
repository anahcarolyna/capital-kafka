package br.com.empresa.consumer;

import br.com.empresa.models.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import br.com.empresa.producer.ReceitaCapitalProducer;

@Component
public class EmpresaConsumer {

    @Autowired
    private ReceitaCapitalProducer producer;

    @KafkaListener(topics = "spec3-ana-carolina-2", groupId = "Carol-empresa")
    public void receber(@Payload Empresa empresa)  {
        System.out.println("Recebi as informações da empresa:" + empresa.getCnpj());
        producer.enviarAoKafka(empresa);
    }
}
